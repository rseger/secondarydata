import numpy
import keras
import matplotlib.pyplot as pyplot
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten

def getSecondary(images):
	secondary = []
	for i in range(len(images)):
		left = 0
		right = 0
		top = 0
		bot = 0
		total = 0
		quad1 = 0
		quad2 = 0
		quad3 = 0
		quad4 = 0
		oddRow = 0
		evenRow = 0
		oddCol = 0
		evenCol = 0
		checkerQuad1 = 0
		checkerQuad2 = 0
		checkerQuad3 = 0
		checkerQuad4 = 0
		prime3 = 0
		prime5 = 0
		prime7 = 0
		prime11 = 0
		prime13 = 0
		prime17 = 0
		prime19 = 0
		prime23 = 0
		power4 = 0
		power6 = 0
		power8 = 0
		power10 = 0
		power12 = 0
		power14 = 0
		power16 = 0
		data = images[i].flatten()
		size = len(data)
		for j in range(size):
			if data[j] > 0:
				total += 1
				qt = False
				qb = False
				ql = False
				qr = False
				cht = False
				chb = False
				chl = False
				chR = False
				if j >= size/2:
					bot += 1
					qb = True
				else:
					top += 1
					qt = True
				col = j%28
				if col >= 14:
					right += 1
					qr = True
				else:
					left += 1
					ql = True
				if col%2 == 0:
					evenCol += 1
					chl = True
				else:
					oddCol += 1
					chR = True
				row = j//28
				if row%2 == 0:
					evenRow += 1
					cht = True
				else:
					oddRow += 1
					chb = True
				if qt and ql: quad1 += 1
				if qt and qr: quad2 += 1
				if qb and ql: quad4 += 1
				if qb and qr: quad3 += 1
				if cht and chl: checkerQuad1 += 1
				if cht and chR: checkerQuad2 += 1
				if chb and chl: checkerQuad4 += 1
				if chb and chR: checkerQuad3 += 1
				if j%3 == 0: prime3 += 1
				if j%5 == 0: prime5 += 1
				if j%7 == 0: prime7 += 1
				if j%11 == 0: prime11 += 1
				if j%13 == 0: prime13 += 1
				if j%17 == 0: prime17 += 1
				if j%19 == 0: prime19 += 1
				if j%23 == 0: prime23 += 1
				if j%4 == 0: power4 += 1
				if j%6 == 0: power6 += 1
				if j%8 == 0: power8 += 1
				if j%10 == 0: power10 += 1
				if j%12 == 0: power12 += 1
				if j%14 == 0: power14 += 1
				if j%16 == 0: power16 += 1
		left = left/size
		right = right/size
		diffSides = abs(left - right)
		top = top/size
		bot = bot/size
		diffStack = abs(top - bot)
		total = total/size
		quad1 = quad1/size
		quad2 = quad2/size
		quad3 = quad3/size
		quad4 = quad4/size
		diffQuadA = abs(quad1 - quad2)
		diffQuadB = abs(quad1 - quad3)
		diffQuadC = abs(quad1 - quad4)
		diffQuadD = abs(quad2 - quad3)
		diffQuadE = abs(quad2 - quad4)
		diffQuadF = abs(quad3 - quad4)
		checkerQuad1 = checkerQuad1/size
		checkerQuad2 = checkerQuad2/size
		checkerQuad3 = checkerQuad3/size
		checkerQuad4 = checkerQuad4/size
		diffCQA = abs(checkerQuad1 - checkerQuad2)
		diffCQB = abs(checkerQuad1 - checkerQuad3)
		diffCQC = abs(checkerQuad1 - checkerQuad4)
		diffCQD = abs(checkerQuad2 - checkerQuad3)
		diffCQE = abs(checkerQuad2 - checkerQuad4)
		diffCQF = abs(checkerQuad3 - checkerQuad4)
		oddRow = oddRow/size
		evenRow = evenRow/size
		diffRows = abs(oddRow - evenRow)
		oddCol = oddCol/size
		evenCol = evenCol/size
		diffCols = abs(oddCol - evenCol)
		diffEven = abs(evenRow - evenCol)
		diffOdd = abs(oddRow - oddCol)
		prime3 = prime3/size
		prime5 = prime5/size
		prime7 = prime7/size
		prime11 = prime11/size
		prime13 = prime13/size
		prime17 = prime17/size
		prime19 = prime19/size
		prime23 = prime23/size
		power4 = power4/size
		power6 = power6/size
		power8 = power8/size
		power10 = power10/size
		power12 = power12/size
		power14 = power14/size
		power16 = power16/size
		notTotal = 1 - total
		notQuad1 = 1 - quad1
		notQuad2 = 1 - quad2
		notQuad3 = 1 - quad3
		notQuad4 = 1 - quad4
		notPrime3 = 1 - prime3
		notPrime5 = 1 - prime5
		notPrime7 = 1 - prime7
		notPrime11 = 1 - prime11
		notPrime13 = 1 - prime13
		notPrime17 = 1 - prime17
		notPrime19 = 1 - prime19
		notPrime23 = 1 - prime23
		notPower4 = 1 - power4
		notPower6 = 1 - power6
		notPower8 = 1 - power8
		notPower10 = 1 - power10
		notPower12 = 1 - power12
		notPower14 = 1 - power14
		notPower16 = 1 - power16

		secondary.append(numpy.array([total, oddRow, evenRow,
										oddCol, evenCol, checkerQuad1,
										checkerQuad2, checkerQuad3,
										checkerQuad4, prime3, prime5, prime7,
										prime11, prime13, prime17, prime19,
										prime23, power4, power6, power8,
										power10, power12, power14, power16,
										notTotal, notQuad1, notQuad2, notQuad3,
										notQuad4, notPrime3, notPrime5, notPrime7,
										notPrime11, notPrime13, notPrime17,
										notPrime19, notPrime23, notPower4,
										notPower6, notPower8, notPower10,
										notPower12, notPower14, notPower16,
										diffCols, diffCQA, diffCQB, diffCQC,
										diffCQD, diffCQE, diffCQF, diffEven,
										diffOdd, diffQuadA, diffQuadB,
										diffQuadC, diffQuadD, diffQuadE, diffQuadF,
										diffRows, diffSides, diffStack]))
		
		"""
		secondary.append(numpy.array([total, oddRow, evenRow,
										oddCol, evenCol, checkerQuad1,
										checkerQuad2, checkerQuad3,
										checkerQuad4, prime3, prime5, prime7,
										prime11, prime13, prime17, prime19,
										prime23, power4, power6, power8,
										power10, power12, power14, power16,
										notTotal, notQuad1, notQuad2, notQuad3,
										notQuad4, notPrime3, notPrime5, notPrime7,
										notPrime11, notPrime13, notPrime17,
										notPrime19, notPrime23, notPower4,
										notPower6, notPower8, notPower10,
										notPower12, notPower14, notPower16]))
		"""
		"""
		secondary.append(numpy.array([total, left, right, top, bot, quad1,
										quad2, quad3, quad4, oddRow, evenRow,
										oddCol, evenCol, checkerQuad1,
										checkerQuad2, checkerQuad3,
										checkerQuad4]))
		"""
		"""
		secondary.append(numpy.array([total, oddRow, evenRow,
										oddCol, evenCol, checkerQuad1,
										checkerQuad2, checkerQuad3,
										checkerQuad4]))
		"""
		"""
		secondary.append(numpy.array([total, oddRow, evenRow,
										oddCol, evenCol, checkerQuad1,
										checkerQuad2, checkerQuad3,
										checkerQuad4, prime3, prime5, prime7,
										prime11, prime13, prime17, prime19,
										prime23]))
		"""
		"""
		secondary.append(numpy.array([total, oddRow, evenRow,
										oddCol, evenCol, checkerQuad1,
										checkerQuad2, checkerQuad3,
										checkerQuad4, prime3, prime5, prime7,
										prime11, prime13, prime17, prime19,
										prime23, power4, power6, power8,
										power10, power12, power14, power16]))
		"""
	return numpy.array(secondary)

def trainSecondary(load=False, joint=False):
	""" define secondary traits
	collect them
	queue them up with the right classifications
	create model
	run above through model
	profit?
	"""
	(trainingData, trainingCat), (testingData, testingCat) = mnist.load_data()
	netSize = 50
	epochs = 100
	numClasses = 10
	if load:
		trainingSecondary = numpy.load("trainSecondary.npy")
		testingSecondary = numpy.load("testImgSecondary.npy")
	else:
		trainingSecondary = getSecondary(trainingData)
		numpy.save("trainSecondary.npy", trainingSecondary)
		testingSecondary = getSecondary(testingData)
		numpy.save("testImgSecondary.npy", testingSecondary)
	if joint:
		trainingData = trainingData.astype('float32')
		testingData = testingData.astype('float32')
		trainingData /= 255
		testingData /= 255
		tmp = []
		for i in range(len(trainingData)):
			tmp.append(numpy.append(trainingData[i].flatten(), trainingSecondary[i]))
		trainingData = numpy.array(tmp)
		tmp = []
		for i in range(len(testingData)):
			tmp.append(numpy.append(testingData[i].flatten(), testingSecondary[i]))
		testingData = numpy.array(tmp)
	else:
		trainingData = trainingSecondary
		testingData = testingSecondary
	print(trainingData[0].shape)
	testingCat = keras.utils.to_categorical(testingCat, numClasses)
	trainingCat = keras.utils.to_categorical(trainingCat, numClasses)

	model = Sequential()
	model.add(Dense(netSize, activation='relu'))
	model.add(Dense(numClasses, activation='softmax'))
	model.compile(loss=keras.losses.categorical_crossentropy,
				optimizer=keras.optimizers.Adadelta(),
				metrics=['accuracy'])
	history = model.fit(trainingData, trainingCat,
			batch_size=128,
			epochs=epochs,
			verbose=1,
			validation_data=(testingData, testingCat))
	score = model.evaluate(testingData, testingCat, verbose=0)
	return score[1], history

if __name__ == "__main__":
	score, history = trainSecondary()
	pyplot.plot(history.history['accuracy'])
	pyplot.plot(history.history['val_accuracy'])
	pyplot.title('Derived Information')
	pyplot.ylabel('accuracy')
	pyplot.xlabel('epoch')
	pyplot.legend(['train', 'test'], loc='upper left')
	print(max(history.history['val_accuracy']))
	pyplot.show()